#!/usr/bin/env python
""" Solution for AirTable backend coding challenge """
################################################################
# Filename: solution.py
# Location: /Users/sivasatish/temp/airtable_sql_evaluator_exercise_v5
# Project :
# Date    : 2018-03-19
# Author  : sivasatish
# Scope   :
# Usage   :
################################################################

import os
import ast
import sys
import copy
import json
import logging
import itertools

###############################
# Constants
###############################
BASH_RCODE_FAILURE = 1
BASH_RCODE_SUCCESS = 0
FUNCTION_RCODE_FAILURE = 0
FUNCTION_RCODE_SUCCESS = 1

TABLE_FILE_SUFFIX = ".table.json"

# Data Types
DATA_TYPE_INT = 0
DATA_TYPE_STRING = 1
DATA_TYPES = [DATA_TYPE_INT, DATA_TYPE_STRING]

OPS_LAMBDA_DICT = {
    ">": (lambda x, y: x > y),
    "<": (lambda x, y: x < y),
    "=": (lambda x, y: x == y),
    "!=": (lambda x, y: x != y),
    ">=": (lambda x, y: x >= y),
    "<=": (lambda x, y: x <= y),
}

logging.basicConfig(format='[ %(levelname)s %(filename)s: %(funcName)10s: %(lineno)4d] %(message)s',
                    level=logging.DEBUG)
log = logging.getLogger(__name__)


###############################
# Function: process_sql_json_file
###############################
def process_sql_json_file(file_name):

    # Parameter type validation
    if not isinstance(file_name, str):
        log.error("Invalid input parameter, file_name must be string")
        return FUNCTION_RCODE_FAILURE

    # Check that if the file_name is valid
    if not os.path.exists(file_name):
        log.error("Filename: {0} doesn't exist, quitting".format(file_name))
        return FUNCTION_RCODE_FAILURE

    log.debug("Processing input file: {0}".format(file_name))
    fp = open(file_name)
    if not fp:
        log.error("Filename: {0} open failed, quitting".format(file_name))
        return FUNCTION_RCODE_FAILURE

    json_data = json.loads(fp.read())

    # Tables names
    try:
        table_names = list(
            set(t_entry["source"]["file"] for t_entry in json_data["from"]))
        log.debug("File: {0}, processing Table names {1}".format(file_name, table_names))
    except KeyError as ke:
        log.execption(ke)
        log.error("Error processing file tables name: {0}".format(file_name))
        return FUNCTION_RCODE_FAILURE

    # Load tables
    table_data = load_tables(table_names)

    # Valid column names
    table_column_map = dict((k, v.get_columns()) for k, v in table_data.items())
    log.debug("Column Map: {0}".format(table_column_map))

    # Get table aliases
    table_alias_map = dict((t_entry["as"], table_data[t_entry["source"]["file"]]) for t_entry in json_data["from"])
    log.debug("Table alias map: {0}".format(table_alias_map))

    ########################
    # Sub-query: From
    ########################
    # Prepare rows using cartesian product if needed
    rows_list = []
    column_list = []
    for t_entry in json_data["from"]:
        current_table = table_data[t_entry["source"]["file"]]
        rows_list.append(current_table.get_rows())
        temp_columns = [copy.copy(col) for col in list(current_table.get_columns().values())]
        for col in temp_columns:
            col.set_alias(t_entry["as"])
        column_list.extend(temp_columns)

    target_rows = list(itertools.product(*rows_list))
    target_rows = [list(itertools.chain(*row)) for row in target_rows]
    log.debug("Colu: {0}".format(column_list))
    log.debug("Rows: {0}".format(target_rows))

    ########################
    # Sub-query: Where
    ########################
    # Apply predicates
    where_query = json_data["where"]
    predicate_count = len(where_query)
    if predicate_count:
        log.debug("Applying predicates")

        for predicate in where_query:

            operation = predicate["op"]
            left_operand = predicate["left"]
            right_operand = predicate["right"]

            log.debug("Left: {0} OP: {1} Right: {2}".format(left_operand, operation, right_operand))

            if operation not in OPS_LAMBDA_DICT.keys():
                print("ERROR: Unsupported operation: {0}".format(operation))

            # Check if the operand is compatible
            left_data_type = get_data_type_from_operand(left_operand, table_column_map, table_alias_map)
            right_data_type = get_data_type_from_operand(right_operand, table_column_map, table_alias_map)
            if left_data_type != right_data_type:
                sys.exit(BASH_RCODE_FAILURE)
            elif left_data_type == DATA_TYPE_STRING and (operation != "=" and operation != "!="):
                sys.exit(BASH_RCODE_FAILURE)

            left_literal_value = left_operand["literal"] if "literal" in left_operand else None
            right_literal_value = right_operand["literal"] if "literal" in right_operand else None

            left_column_index = None
            right_column_index = None
            if not left_literal_value:
                left_column_index = get_column_index(left_operand, column_list)
            if not right_literal_value:
                right_column_index = get_column_index(right_operand, column_list)

            filtered_rows = []
            for row in target_rows:

                left_value = left_literal_value if left_literal_value else row[left_column_index]
                right_value = right_literal_value if right_literal_value else row[right_column_index]

                # log.debug("LL: {0} RL: {1} Row: {2}".format(left_value, right_value, row))

                if OPS_LAMBDA_DICT[operation](left_value, right_value):
                    filtered_rows.append(row)

            target_rows = filtered_rows

    ########################
    # Sub-query: select
    ########################
    # Prepare column header
    select_subquery = json_data["select"]
    op_column_names = []
    target_col_list = []
    max_col_width = {}
    op_column_metadata = []

    for index, op_col in enumerate(select_subquery):
        max_col_width[index] = len(op_col["as"])
        column_metadata_index = get_column_index(op_col["source"], column_list)
        op_column_metadata.append(column_list[column_metadata_index])
        op_column_names.append(op_col["as"])
        target_col_list.append(column_metadata_index)

    log.debug("Target Col List: {0}".format(target_col_list))
    log.debug("Target Col Names: {0}".format(op_column_names))

    col_count = 0
    if len(target_rows):
        col_count = len(target_rows[0])

    op_data = []
    for row in target_rows:
        current_row = []
        out_col_index = 0
        for index in range(col_count):
            if index in target_col_list:
                current_row.append(row[index])
                value_len = len(str(row[index]))
                max_col_width[out_col_index] = max_col_width[out_col_index] if max_col_width[out_col_index] > value_len else value_len
                out_col_index = out_col_index + 1
        op_data.append(current_row)

    # Print Columns
    column_line = ""
    for index, col in enumerate(op_column_metadata):
        col_op_name = op_column_names[index]
        if col.get_data_type() == DATA_TYPE_INT:
            column_line = column_line + col_op_name.rjust(max_col_width[index])
        else:
            column_line = column_line + col_op_name.ljust(max_col_width[index])
        column_line = column_line + " | "
    column_line = column_line[:-3]
    print(column_line)
    print('-' * len(column_line))

    op_column_count = len(op_column_metadata)
    for row in op_data:
        for index, col_value in enumerate(row):
            col_dt = op_column_metadata[index].get_data_type()
            if col_dt == DATA_TYPE_INT:
                print(str(col_value).rjust(max_col_width[index]), end="")
            else:
                print(str(col_value).ljust(max_col_width[index]), end="")
            if index != op_column_count-1:
                print(" | ", end="")
        print()

    return FUNCTION_RCODE_SUCCESS


###############################
# Class: get_column_index
###############################
def get_column_index(predicate_operand, column_list):

    table_name = predicate_operand["column"]["table"]
    column_name = predicate_operand["column"]["name"]

    got_match = False
    rvalue = None
    target_table = None
    for index, column in enumerate(column_list):

        if not column.get_name() == column_name:
            continue

        if table_name:
            if column.get_alias() == table_name:
                rvalue = index
                break
        else:
            if got_match:
                print("Error: Column reference \"{0}\" is ambiguous; "
                      "present in multiple tables: \"{1}\", \"{2}\"."
                      .format(column_name, target_table, column.table_name))
                sys.exit(BASH_RCODE_FAILURE)
            got_match = True
            target_table = column.table_name
            rvalue = index

    # if not rvalue:
    #     print("ERROR: Unknown table name \"{}\".".format(table_name))
    #     sys.exit(BASH_RCODE_FAILURE)

    log.debug("Predicate: {0} CI: {1} CN: {2}".format(predicate_operand, rvalue, column_name))
    return rvalue

###############################
# Class: get_data_type_from_operand
###############################
def get_data_type_from_operand(predicate_operand, table_column_map, table_alias_map):

    if "literal" in predicate_operand:
        if isinstance(predicate_operand["literal"], int):
            return DATA_TYPE_INT
        elif isinstance(predicate_operand["literal"], str):
            return DATA_TYPE_STRING
        else:
            return None
    else:
        alias_name = predicate_operand["column"]["table"]
        column_name = predicate_operand["column"]["name"]

        if alias_name:
            table = table_alias_map[alias_name]
            columns = table_column_map[table.get_name()]
            if column_name in columns:
                return columns[column_name].data_type
            else:
                print("Error: Column reference \"{0}\" is invalid for table \"{1}\"".format(column_name, table))
        else:
            rtype = None
            found_match = False
            target_table = None
            for table, columns in table_column_map.items():

                columns = list(columns.keys())
                if column_name in columns:

                    if found_match:
                        print("Error: Column reference \"{0}\" is ambiguous; "
                              "present in multiple tables: \"{1}\", \"{2}\"."
                              .format(column_name, target_table, table))

                    target_table = table
                    rtype = table_column_map[table][column_name].data_type
            return rtype


###############################
# Class: Column
###############################
class Column:

    def __init__(self, name, table_name, data_type, column_number, cardinality, distribution_ratio):
        self.name = name
        self.alias_table_name = None
        self.table_name = table_name
        self.cardinality = cardinality
        self.column_number = column_number
        self.distribution_ratio = distribution_ratio

        if data_type == 'int':
            self.data_type = DATA_TYPE_INT
        elif data_type == 'str':
            self.data_type = DATA_TYPE_STRING
        else:
            log.error("Unsupported datatype: {}".format(data_type))
            raise Exception("Unsupported datatype")

    def get_name(self):
        return self.name

    def set_alias(self, alias):
        self.alias_table_name = alias

    def get_alias(self):
        return self.alias_table_name

    def get_data_type(self):
        return self.data_type

    def __repr__(self):
        return "Table: {0}, Name: {1},  Type: {2}, Column Number: {3}, " \
               "Cardinality: {4} Distribution Ratio: {5} Alias: {6} ID: {7}\n"\
        .format(
            self.table_name,
            self.name,
            self.data_type,
            self.column_number,
            self.cardinality,
            self.distribution_ratio,
            self.alias_table_name,
            id(self)
        )


###############################
# Class: Table
###############################
class Table:

    _COLUMN_METADATA_ROW_NUMBER = 0
    _ROWS_START_INDEX = 1

    _COLUMN_INDEX_NAME = 0
    _COLUMN_INDEX_TYPE = 1

    def __init__(self, table_name):

        self.rows = []
        self.__row_count = 0
        self.__table_name = ""
        self.__column_names = []
        self.__column_order = {}
        self.columns = {}

        log.debug("Processing table: {0}".format(table_name))
        self.__table_name = table_name

        # Open file
        file_name = table_name + TABLE_FILE_SUFFIX

        # Check filename
        if not os.path.exists(file_name):
            print("ERROR: Unknown table name \"{0}\".".format(table_name))
            sys.exit(BASH_RCODE_FAILURE)

        log.debug("Processing table file: {0}".format(file_name))
        fp = open(file_name)
        fd = fp.read()

        table_data = ast.literal_eval(fd)
        if not len(table_data):
            log.error("Invalid table file for file: {0}, skipping".format(file_name))
            raise Exception("Invalid table file")

        # Rows & row_count
        self.rows = table_data[Table._ROWS_START_INDEX:]
        self.__row_count = len(self.rows)
        log.debug("Row processing finished")

        # Columns
        #   - metadata {name:Column(name, type, column_number)}
        #  -  column names
        try:
            column_metadata = table_data[Table._COLUMN_METADATA_ROW_NUMBER]
            for index, col in enumerate(column_metadata):

                # Column cardinality
                column_cardinality = len(set([row[index] for row in self.rows]))

                self.columns[col[Table._COLUMN_INDEX_NAME]] = \
                    Column(name=col[Table._COLUMN_INDEX_NAME],
                           table_name=table_name,
                           data_type=col[Table._COLUMN_INDEX_TYPE],
                           column_number=index,
                           cardinality=column_cardinality,
                           distribution_ratio=column_cardinality/self.__row_count)

            self.__column_names = list(self.columns.keys())
            # Duplicate column names
            if len(column_metadata) != len(self.columns):
                log.error("Duplicate column names, failing".format(file_name))
                print("ERROR: Duplicate error names")
                raise Exception("Duplicate column names")
            log.debug(self.columns)
            log.debug("Column processing finished")
        except IndexError as ie:
            log.exeption(ie)
            log.error("Column processing failed for file: {0}, skipping".format(file_name))
            raise Exception("File processing failed")

        print("- Loaded \"{0}\", {1} rows.".format(file_name, self.__row_count))

    def get_name(self):
        return self.__table_name

    def get_rows(self):
        return self.rows

    def get_columns(self):
        return self.columns

    def get_column_names(self):
        return self.__column_names

    def __repr__(self):
        return "Table Name: \'" + self.__table_name + "\' Row Count: " + str(self.__row_count)


###############################
# Function: load_tables
###############################
def load_tables(table_names):

    log.debug("BEGIN: Table names: {0}".format(table_names))

    tables = {}
    for table in table_names:
        log.debug("Loading table: {}".format(table))
        tables[table] = Table(table)

    return tables


###############################
# Function: Solution
###############################
def solution():
    """ Solution function """

    try:
        # Validate arguments
        file_name = process_arguments()
        if not file_name:
            log.error("Error when processing command-line arguments, quitting")
            sys.exit(BASH_RCODE_FAILURE)

        # Process file
        rcode = process_sql_json_file(file_name)
        if rcode != FUNCTION_RCODE_SUCCESS:
            log.error("Failed to process SQL JSON file, quitting")
            sys.exit(BASH_RCODE_FAILURE)
        elif rcode == FUNCTION_RCODE_SUCCESS:
            return BASH_RCODE_SUCCESS
    except Exception as e:
        log.exception(e)
        log.error("Exception occured, quitting")
        return BASH_RCODE_FAILURE


###############################
# Function: print_help
###############################
def print_help():
    """ Print usage of the solution """
    print("Usage: ", __file__, " <sql_json_file>")


###############################
# Function: process_arguments
###############################
def process_arguments():
    """ Process command line arguments """

    VALID_ARG_COUNT = 2
    ARG_INDEX_FILE_NAME = 1

    if len(sys.argv) != VALID_ARG_COUNT:
        log.error("Invalid usage: Quitting")
        print_help()
        sys.exit(BASH_RCODE_FAILURE)

    # Check if the file is valid
    file_name = sys.argv[ARG_INDEX_FILE_NAME]
    if not os.path.exists(file_name):
        log.error("Input SQL-JSON file doesn't exist, quitting")
        sys.exit(BASH_RCODE_FAILURE)

    return file_name


###############################
# Function: main
###############################
def main():
    """ Place holder function """
    return solution()

if __name__ == "__main__":
    sys.exit(main())
